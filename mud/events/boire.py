from .event import Event2

class BoireEvent(Event2):
    NAME = "boire"

    def perform(self):
        print(self.object.has_prop("ferme"))
        if not self.object.has_prop("buvable"):
            self.add_prop("object-not-buvable")
            return self.boire_failed()
        if not self.object.has_prop("ferme"):
            if self.object in self.actor:
                self.object.move_to(None)
                self.inform("boire")
                self.actor.alcool+=1
        else:
            return self.boire_failed()

    def boire_failed(self):
        self.fail()
        self.inform("boire.failed")
