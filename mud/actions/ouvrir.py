# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action3
from mud.events import OuvrirAvecEvent

class OuvrirAvecAction(Action3):
    EVENT = OuvrirAvecEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "ouvrir-avec"